const { default: axios } = require("axios");
const express = require("express");
const app = express();
const port = 3000;

const RESULTS_PER_PAGE = 10;
const BASE_URL = "https://swapi.dev/api";
const PLANETS_URL = `${BASE_URL}/planets`;
const PEOPLE_URL = `${BASE_URL}/people`;

const SpeedOption = {
  SLOW: "slow",
  FAST: "fast",
};

// Change DEFAULT_SPEED to try different data fetching strategies
const DEFAULT_SPEED = SpeedOption.FAST;

const sortPeople = (people, sortBy) => {
  if (sortBy === "name") {
    people.sort((a, b) => {
      if (a[sortBy] < b[sortBy]) {
        return -1;
      } else if (a[sortBy] > b[sortBy]) {
        return 1;
      }
      return 0;
    });
  }
  if (sortBy === "height" || sortBy === "mass") {
    people.sort((a, b) => {
      if (a[sortBy] === "unknown") {
        return 1;
      } else if (b[sortBy] === "unknown") {
        return -1;
      }
      return (
        parseInt(a[sortBy].replace(/,/g, ""), 10) -
        parseInt(b[sortBy].replace(/,/g, ""), 10)
      );
    });
  }

  return people;
};

const getAllViaPagination = async (url) => {
  let swapiResponse = await axios.get(url);
  const results = swapiResponse.data.results;
  while (swapiResponse.data.next) {
    swapiResponse = await axios.get(swapiResponse.data.next);
    results.push(...swapiResponse.data.results);
  }
  return results;
};

const getAllConcurrently = async (url) => {
  const swapiResponse = await axios.get(url);
  const results = swapiResponse.data.results;
  const emptyArray = new Array(
    Math.ceil(swapiResponse.data.count / RESULTS_PER_PAGE) - 1
  ).fill();
  const allData = await Promise.all(
    emptyArray.map(async (_, index) => {
      const response = await axios.get(`${url}?page=${index + 2}`);
      return response.data.results;
    })
  );
  results.push(...allData.flat());
  return results;
};

const getAll = async (url, speed = DEFAULT_SPEED) => {
  if (speed === SpeedOption.FAST) {
    return await getAllConcurrently(url);
  } else if (speed === SpeedOption.SLOW) {
    return await getAllViaPagination(url);
  }
};

const assignResidentNamesFast = async (planets) => {
  const allPeople = await getAll(PEOPLE_URL, SpeedOption.FAST);
  return planets.map((planet) => {
    const residents = planet.residents.map((resident) => {
      const person = allPeople.find((person) => person.url === resident);
      return person.name;
    });
    return { ...planet, residents: residents };
  });
};

const assignResidentNamesSlow = async (planets) => {
  return await Promise.all(
    planets.map(async (planet) => {
      const residents = await Promise.all(
        planet.residents.map(async (resident) => {
          const personResponse = await axios.get(resident);
          return personResponse.data.name;
        })
      );
      return { ...planet, residents: residents };
    })
  );
};

const assignResidentNames = async (planets, speed = DEFAULT_SPEED) => {
  if (speed === SpeedOption.FAST) {
    return await assignResidentNamesFast(planets);
  } else if (speed === SpeedOption.SLOW) {
    return await assignResidentNamesSlow(planets);
  }
};

app.get("/people", async (req, res) => {
  const people = await getAll(PEOPLE_URL);
  const sortedPeople = sortPeople(people, req.query.sortBy);
  res.send(sortedPeople);
});

app.get("/planets", async (req, res) => {
  const planets = await getAll(PLANETS_URL);
  const planetsWithResidentNames = await assignResidentNames(planets);
  res.send(planetsWithResidentNames);
});

app.listen(port, () => {
  console.log(`SOLO Node Exercise listening on port ${port}`);
});
